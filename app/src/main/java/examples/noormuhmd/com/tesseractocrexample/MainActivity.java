package examples.noormuhmd.com.tesseractocrexample;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    //to display result
    TextView result;
    //to display preview of image
    ImageView mImageView;
    //Buttons for take using camera and choose existing options
    Button take;
    Button choose;
    //Request codes for two options
    static final int REQUEST_IMAGE_CAPTURE=1;
    static final int REQUEST_IMAGE_CHOOSE=2;
    //to store image captured by camera
    Bitmap imageTaken;
    //to store image chosen from device
    Bitmap imageChosen;
    //TessBaseAPI object
    private TessBaseAPI mTess;
    //datapath to the folder containing language file
    String datapath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialising objects of Textview, ImageView and Buttons
        result=(TextView)findViewById(R.id.result);
        mImageView=(ImageView)findViewById(R.id.mImageView);
        take=(Button)findViewById(R.id.take);
        choose=(Button)findViewById(R.id.choose);

        //Creating file path
        datapath = getFilesDir()+"/tesseract/";

        //checking the file
        checkFile(new File(datapath + "tessdata/"));

        //Setting lang as english. You can change it according to which language you are using
        String lang = "eng";

        //Initialising Tesseract API
        mTess = new TessBaseAPI();
        mTess.init(datapath, lang);

        take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Take button clicked;Performing Intent to take picture
                dispatchTakePictureIntent();
            }
        });

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Choose button clicked;Performing intent to choose picture
                dispatchChoosePictureIntent();
            }
        });

    }

    //used to capture image using camera
    private void dispatchTakePictureIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!=null)
            startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE);
    }

    //used to choose existing image from device
    private void dispatchChoosePictureIntent(){
        Intent choosePictureIntent = new Intent();
        choosePictureIntent.setType("image/*");
        choosePictureIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(choosePictureIntent,"Select picture"),REQUEST_IMAGE_CHOOSE);
    }

    //This method is executed from the StartActivityForResult() call from above two methods.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            //The user opt to capture image using camera.
            Bundle extras = data.getExtras();
            //getting the image
            imageTaken=(Bitmap)extras.get("data");
            //displayting image preview
            mImageView.setImageBitmap(imageTaken);
            //Processing image
            processImage(imageTaken);
        }
        else if(requestCode==REQUEST_IMAGE_CHOOSE && resultCode == RESULT_OK){
            //The user opt to choose existing image from device.
            Uri uri = data.getData();
            try{
                //getting the image
                imageChosen = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                //displaying image preview
                mImageView.setImageBitmap(imageChosen);
                //processing image
                processImage(imageChosen);
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    //Here the input image is processed using the TessBaseAPI object
    public void processImage(Bitmap input) {
        String OCRresult = null;
        //Setting input image
        mTess.setImage(input);
        //Getting the result
        OCRresult = mTess.getUTF8Text();
        //Displaying the result in the TextView
        result.setText(OCRresult);
    }

    //Used to copy test data to device
    private void copyFiles() {
        Log.d("Status","copying files");
        try {
            //location we want the file to be at
            String filepath = datapath + "/tessdata/eng.traineddata";

            //get access to asset manager
            AssetManager assetManager = getAssets();

            //Open byte streams for w/r
            InputStream instream = assetManager.open("tessdata/eng.traineddata");
            OutputStream outstream = new FileOutputStream(filepath);

            //Copy the file to the location specified
            byte[] buffer = new byte[1024];
            int read;
            while((read = instream.read(buffer)) != -1){
                outstream.write(buffer, 0, read);
            }
            outstream.flush();
            outstream.close();
            instream.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    //Used to check whether the file already on device
    private void checkFile(File dir){
        //directory not exists, but we can create one
        if(!dir.exists() && dir.mkdirs()){
            copyFiles();
        }
        if(dir.exists()){
            //Directory exists but there is no file
            String dataFilepath = datapath+"/tessdata/eng.traineddata";
            File dataFile = new File(dataFilepath);
            if(!dataFile.exists()){
                copyFiles();
            }
        }
    }
}
