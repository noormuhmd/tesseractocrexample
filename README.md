This is a simple android application that recognise text from images using Tesseract API. 

The app primarily have two options - 
   1.choose existing image 2.take new image using camera.
After getting the image from the above, it will process the image using tess-two. 

I have used tess-two 8.0.0. You can compile with latest version available. 

The app currently supports English only. 
You can use any language by using trained data of that language. Get them from here https://github.com/tesseract-ocr/tessdata